import nodemailer from 'nodemailer';
import 'dotenv/config';

const config = {
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD
    }
};

const sendMail = async () => {
    const transporter = nodemailer.createTransport(config);

    // Verify connection configuration
    transporter.verify(async function (error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log('Server is ready to take our messages');

            // send mail with defined transport object
            const info = await transporter.sendMail({
                from: `"Fred Foo 👻" <${process.env.MAIL_FROM_EMAIL}>`, // sender address
                to: 'bar@example.com, baz@example.com', // list of receivers
                subject: 'Hello ✔', // Subject line
                text: 'Hello world?', // plain text body
                html: '<b>Hello world?</b>' // html body
            }).catch(err => {
                console.log(err);
            });

            console.log('Message sent: %s', info.messageId);
        }
    });
};

export default sendMail;
