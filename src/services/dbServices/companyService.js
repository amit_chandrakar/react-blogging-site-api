import Company from '../../models/company-model.js';
import HttpError from '../httpErrorService.js';

/**
 * Stores a new company in the database.
 * @param {Object} data - The data for the new company.
 * @param {string} data.appName - The name of the company's app.
 * @param {string} data.dateFormat - The date format for the company.
 * @param {string} data.timeFormat - The time format for the company.
 * @param {string} data.language - The language for the company.
 * @param {boolean} data.miniDrawer - Whether the company uses a mini drawer.
 * @returns {Promise<Object>} - The newly created company object.
 * @throws {HttpError} - If there was an error saving the company to the database.
 */
const store = async (data) => {
    const { appName, dateFormat, timeFormat, language, miniDrawer } = data;

    const newCompany = new Company({
        appName,
        dateFormat,
        timeFormat,
        language,
        miniDrawer
    });

    try {
        await newCompany.save();
        return { company: newCompany };
    } catch (err) {
        throw new HttpError(err, 500);
    }
};

/**
 * Updates a company's information in the database.
 * @param {string} companyId - The ID of the company to update.
 * @param {Object} req - The request object.
 * @param {Object} requestBody - The request body containing the updated company information.
 * @returns {Promise<{company: Object}>} - A promise that resolves to an object containing the updated company information.
 * @throws {HttpError} - Throws an error if there was a problem updating the company information.
 */
const update = async (companyId = null, req, requestBody) => {
    const { appName, dateFormat, timeFormat, language, miniDrawer } = requestBody;

    // Check if file is not selected
    const { filename, path } = req.file ? req.file : {};

    let company;

    try {
        company = await Company.findOne();
    } catch (err) {
        throw new HttpError(err, 500);
    }

    company.appName = appName;
    company.dateFormat = dateFormat;
    company.timeFormat = timeFormat;
    company.language = language;
    company.miniDrawer = miniDrawer;
    company.logo = req.file ? filename : company.logo;
    company.path = req.file ? path : company.path;

    try {
        await company.save();
        return { company };
    } catch (err) {
        throw new HttpError(err, 500);
    }
};

/**
 * Finds a company in the database.
 *
 * @returns {Promise<{ company: Object }>} A promise that resolves to an object containing the company.
 * @throws {HttpError} If there was an error while finding the company.
 */
const find = async () => {
    let company;

    try {
        company = await Company.findOne();
    } catch (err) {
        throw new HttpError(err, 500);
    }

    return { company };
};

export default { store, update, find };
