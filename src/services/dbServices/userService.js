import HttpError from '../../services/httpErrorService.js';
import User from '../../models/user-model.js';
import { LIVE_IMAGE_URL } from '../../utils/constants.js';
import mongoose from 'mongoose';

const DEFAULTS = {
    LIMIT: 10,
    SORT: 'name',
    STATUS: 'All',
    NAME: 'All'
};

/**
 * Retrieves a list of users based on the provided query parameters.
 * @async
 * @function
 * @param {Object} req - The request object.
 * @returns {Promise<Object>} - The response object containing the list of users, total count, page number, and limit.
 * @throws {HttpError} - If there was an error while retrieving the users.
 */
const get = async (req) => {
    let users;

    try {
        const page = parseInt(req.query.page, 10) - 1 || 0;
        const limit = parseInt(req.query.limit, 10) || DEFAULTS.LIMIT;
        const search = req.query.search || '';
        const sort = req.query.sort || DEFAULTS.SORT;
        const name = req.query.name || DEFAULTS.NAME;
        const status = req.query.status || DEFAULTS.STATUS;

        const getBaseAggregation = () => {
            return User.find({ name: { $regex: search, $options: 'i' } }, '-password')
                .where(name !== DEFAULTS.NAME ? { name } : {})
                .where(status !== DEFAULTS.STATUS ? { status } : {});
        };

        users = await getBaseAggregation()
            .skip(page * limit)
            .limit(limit)
            .sort({ [sort]: 1 })
            .exec();

        const total = await getBaseAggregation().countDocuments();

        return {
            status: 'success',
            total,
            page: page + 1,
            limit,
            users
        };
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Stores a new user in the database.
 * @param {Object} data - The user data to be stored.
 * @param {string} data.name - The name of the user.
 * @param {string} data.email - The email of the user.
 * @param {string} data.password - The password of the user.
 * @param {string} data.dob - The date of birth of the user.
 * @param {string} data.gender - The gender of the user.
 * @param {string} data.address - The address of the user.
 * @param {string} data.mobile - The mobile number of the user.
 * @returns {Promise<Object>} - The newly created user object.
 * @throws {HttpError} - If there was an error while saving the user to the database.
 */
const store = async (data) => {
    const { name, email, password, dob, gender, address, mobile } = data;

    const user = new User({
        name, email, password, dob, gender, mobile, address, image: LIVE_IMAGE_URL
    });

    try {
        await user.save();
    } catch (err) {
        throw new HttpError(err.message, 500);
    }

    return user;
};

/**
 * Finds a user by their ID.
 * @param {string} userId - The ID of the user to find.
 * @returns {Promise<object>} - A promise that resolves with the user object if found.
 * @throws {HttpError} - If the user ID is invalid or the user is not found.
 */
const find = async (userId) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(userId)) {
            throw new HttpError('Invalid user ID format', 400);
        }

        userId = mongoose.Types.ObjectId(userId);
        const user = await User.findById(userId);

        if (!user) {
            throw new HttpError('User not found', 404);
        }

        return user;
    } catch (err) {
        // If the error is already an HttpError, throw it as-is
        if (err instanceof HttpError) {
            throw err;
        }
        // Otherwise, wrap it in a generic HttpError
        throw new HttpError(err.message, 500);
    }
};

/**
 * Updates a user with the given data.
 *
 * @param {string} userId - The ID of the user to update.
 * @param {Object} data - The data to update the user with.
 * @param {string} data.name - The name of the user.
 * @param {string} data.email - The email of the user.
 * @param {string} data.password - The password of the user.
 * @param {string} data.dob - The date of birth of the user.
 * @param {string} data.gender - The gender of the user.
 * @param {string} data.address - The address of the user.
 * @param {string} data.mobile - The mobile number of the user.
 * @returns {Promise<Object>} The updated user object.
 * @throws {HttpError} If there was an error updating the user.
 */
const update = async (userId, data) => {
    const { name, email, password, dob, gender, address, mobile } = data;

    // eslint-disable-next-line prefer-const
    let user = await find(userId);
    user.name = name;
    user.email = email;
    user.password = password;
    user.dob = dob;
    user.gender = gender;
    user.address = address;
    user.mobile = mobile;

    try {
        await user.save();
        return user;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Deletes a user from the database.
 * @param {string} userId - The ID of the user to delete.
 * @returns {Promise<boolean>} - A Promise that resolves to true if the user was deleted successfully.
 * @throws {HttpError} - If there was an error deleting the user.
 */
const destroy = async (userId) => {
    try {
        await User.findById(userId).deleteOne().exec();
        return true;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Counts the total number of users in the database.
 *
 * @returns {Promise<number>} The total number of users.
 * @throws {HttpError} If there's an error while counting the users.
 */
const count = async () => {
    let totalUsers = 0;

    try {
        // Count total categories
        totalUsers = await User.countDocuments();
    } catch (err) {
        throw new HttpError(err, 500);
    }

    return totalUsers;
};

export default {
    get,
    store,
    find,
    update,
    destroy,
    count
};
