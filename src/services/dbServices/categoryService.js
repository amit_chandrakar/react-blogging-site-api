import HttpError from '../../services/httpErrorService.js';
import mongoose from 'mongoose';
import Category from '../../models/category-model.js';

const DEFAULTS = {
    LIMIT: 10,
    SORT: 'name',
    STATUS: 'All',
    NAME: 'All'
};

/**
 * Retrieves a list of categories based on the provided query parameters.
 * @async
 * @function
 * @param {Object} req - The request object.
 * @returns {Promise<Object>} - The object containing the categories, total count, page number, and limit.
 * @throws {HttpError} - If there is an error while retrieving the categories.
 */
const get = async (req) => {
    let categories;

    try {
        const page = parseInt(req.query.page, 10) - 1 || 0;
        const limit = parseInt(req.query.limit, 10) || DEFAULTS.LIMIT;
        const search = req.query.search || '';
        const sort = req.query.sort || DEFAULTS.SORT;
        const name = req.query.name || DEFAULTS.NAME;
        const status = req.query.status || DEFAULTS.STATUS;

        const getBaseAggregation = () => {
            return Category.find({ name: { $regex: search, $options: 'i' } })
                .where(name !== DEFAULTS.NAME ? { name } : {})
                .where(status !== DEFAULTS.STATUS ? { status } : {});
        };

        categories = await getBaseAggregation()
            .skip(page * limit)
            .limit(limit)
            .sort({ [sort]: 1 })
            .exec();

        const total = await getBaseAggregation().countDocuments();

        return {
            status: 'success',
            total,
            page: page + 1,
            limit,
            categories
        };
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Stores a new category in the database.
 * @param {Object} data - The data for the new category.
 * @param {string} data.name - The name of the category.
 * @param {string} data.status - The status of the category.
 * @returns {Promise<Object>} - The newly created category object.
 * @throws {HttpError} - If there was an error saving the category to the database.
 */
const store = async (data) => {
    const { name, status } = data;

    const category = new Category({
        name, status
    });

    try {
        await category.save();
    } catch (err) {
        throw new HttpError(err.message, 500);
    }

    return category;
};

/**
 * Finds a category by its ID.
 * @param {string} categoryId - The ID of the category to find.
 * @returns {Promise<Category>} A promise that resolves with the found category.
 * @throws {HttpError} If the category ID is invalid or the category is not found.
 */
const find = async (categoryId) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(categoryId)) {
            throw new HttpError('Invalid Category ID format', 400);
        }

        categoryId = mongoose.Types.ObjectId(categoryId);
        const category = await Category.findById(categoryId);

        if (!category) {
            throw new HttpError('Category not found', 404);
        }

        return category;
    } catch (err) {
        // If the error is already an HttpError, throw it as-is
        if (err instanceof HttpError) {
            throw err;
        }
        // Otherwise, wrap it in a generic HttpError
        throw new HttpError(err.message, 500);
    }
};

/**
 * Updates a category with the given categoryId and data.
 *
 * @param {string} categoryId - The ID of the category to update.
 * @param {object} data - The data to update the category with.
 * @param {string} data.name - The new name of the category.
 * @param {string} data.status - The new status of the category.
 * @returns {Promise<object>} - The updated category object.
 * @throws {HttpError} - If there was an error while saving the category.
 */
const update = async (categoryId, data) => {
    const { name, status } = data;

    // eslint-disable-next-line prefer-const
    let category = await find(categoryId);
    category.name = name;
    category.status = status;

    try {
        await category.save();
    } catch (err) {
        throw new HttpError(err.message, 500);
    }

    return category;
};

/**
 * Deletes a category from the database.
 * @param {string} categoryId - The ID of the category to delete.
 * @returns {Promise<boolean>} - A Promise that resolves to true if the category was successfully deleted.
 * @throws {HttpError} - If there was an error deleting the category.
 */
const destroy = async (categoryId) => {
    try {
        await Category.findById(categoryId).deleteOne().exec();
        return true;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

export default {
    get,
    store,
    find,
    update,
    destroy
};
