import HttpError from '../../services/httpErrorService.js';
import Post from '../../models/post-model.js';
import slugify from 'slugify';
import mongoose from 'mongoose';

const DEFAULTS = {
    LIMIT: 10,
    SORT: 'name',
    STATUS: 'All',
    NAME: 'All'
};

/**
 * Retrieves posts based on the provided query parameters.
 * @async
 * @function
 * @param {Object} req - The request object.
 * @param {Object} req.query - The query parameters.
 * @param {number} [req.query.page=1] - The page number to retrieve.
 * @param {number} [req.query.limit=10] - The maximum number of posts to retrieve per page.
 * @param {string} [req.query.search=''] - The search string to filter posts by title.
 * @param {string} [req.query.sort='createdAt'] - The field to sort the posts by.
 * @param {string} [req.query.title=''] - The title of the posts to retrieve.
 * @param {string} [req.query.status='published'] - The status of the posts to retrieve.
 * @returns {Promise<Object>} An object containing the retrieved posts and pagination information.
 * @throws {HttpError} If an error occurs while retrieving the posts.
 */
const get = async (req) => {
    let posts;

    try {
        const page = parseInt(req.query.page, 10) - 1 || 0;
        const limit = parseInt(req.query.limit, 10) || DEFAULTS.LIMIT;
        const search = req.query.search || '';
        const sort = req.query.sort || DEFAULTS.SORT;
        const title = req.query.title || DEFAULTS.NAME;
        const status = req.query.status || DEFAULTS.STATUS;

        const filter = {
            title: { $regex: search, $options: 'i' },
            $or: [
                { title: title !== DEFAULTS.NAME ? title : { $exists: true } },
                { status: status !== DEFAULTS.STATUS ? status : { $exists: true } }
            ]
        };

        const getBaseAggregation = () => {
            return Post.aggregate([
                {
                    $lookup: {
                        from: 'categories',
                        localField: 'categoryId',
                        foreignField: '_id',
                        as: 'category'
                    }
                },
                {
                    $lookup: {
                        from: 'subcategories',
                        localField: 'subCategoryId',
                        foreignField: '_id',
                        as: 'subcategory'
                    }
                },
                {
                    $match: filter
                }
            ]);
        };

        posts = await getBaseAggregation()
            .skip(page * limit)
            .limit(limit)
            .sort({ [sort]: 1 })
            .exec();

        const total = await Post.countDocuments(filter);

        return {
            status: 'success',
            total,
            page: page + 1,
            limit,
            posts
        };
    } catch (err) {
        throw new HttpError(err, 500);
    }
};

/**
 * Store a new post in the database.
 *
 * @param {Object} data - The data for the new post.
 * @param {string} data.categoryId - The ID of the category the post belongs to.
 * @param {string} data.subCategoryId - The ID of the subcategory the post belongs to.
 * @param {string} data.title - The title of the post.
 * @param {string} data.status - The status of the post.
 * @param {string} data.subTitle - The subtitle of the post.
 * @param {string} data.longDescription - The long description of the post.
 * @returns {Promise<{post: Object}>} - The newly created post.
 * @throws {HttpError} - If there was an error while saving the post.
 */
const store = async (data) => {
    const { categoryId, subCategoryId, title, status, subTitle, longDescription } = data;

    const slug = slugify(title);

    const newPost = new Post({
        categoryId,
        subCategoryId,
        title,
        status,
        subTitle,
        longDescription,
        slug
    });

    try {
        await newPost.save();
        return { post: newPost };
    } catch (err) {
        throw new HttpError(err, 500);
    }
};

/**
 * Finds a post by its ID and returns it with its category and subcategory details.
 * @param {string} postId - The ID of the post to be found.
 * @returns {Promise<{post: Object}>} - The post object with its category and subcategory details.
 * @throws {HttpError} - If there's an error while finding the post.
 */
const find = async (postId) => {
    let post;
    const postIdObject = mongoose.Types.ObjectId(postId);

    try {
        post = await Post.aggregate([
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $lookup: {
                    from: 'subcategories',
                    localField: 'subCategoryId',
                    foreignField: '_id',
                    as: 'subCategory'
                }
            },
            {
                $match: { _id: postIdObject }
            },
            {
                $limit: 1
            }
        ]);

        post = post[0];
    } catch (err) {
        throw new HttpError(err, 500);
    }

    return { post };
};

/**
 * Finds a post by its slug.
 *
 * @param {string} slug - The slug of the post to find.
 * @returns {Promise<{post: Object}>} - A promise that resolves to an object containing the post.
 * @throws {HttpError} - If an error occurs while finding the post.
 */
const findBySlug = async (slug) => {
    let post;

    try {
        post = await Post.aggregate([
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $lookup: {
                    from: 'subcategories',
                    localField: 'subCategoryId',
                    foreignField: '_id',
                    as: 'subcategory'
                }
            },
            {
                $match: { slug }
            }
        ]);

        post = post[0];
    } catch (err) {
        throw new HttpError(err, 500);
    }

    return { post };
};

/**
 * Update a post by ID
 * @param {string} postId - The ID of the post to update
 * @param {object} data - The data to update the post with
 * @param {string} data.categoryId - The ID of the category the post belongs to
 * @param {string} data.subCategoryId - The ID of the subcategory the post belongs to
 * @param {string} data.title - The title of the post
 * @param {string} data.status - The status of the post (draft, published, etc.)
 * @param {string} data.subTitle - The subtitle of the post
 * @param {string} data.longDescription - The long description of the post
 * @returns {object} - The updated post object
 * @throws {HttpError} - If there is an error while updating the post
 */
const update = async (postId, data) => {
    const { categoryId, subCategoryId, title, status, subTitle, longDescription } = data;

    let post;

    try {
        post = await Post.findById(postId);
    } catch (err) {
        throw new HttpError(err, 500);
    }

    const slug = slugify(title);

    post.categoryId = categoryId;
    post.subCategoryId = subCategoryId;
    post.title = title;
    post.subTitle = subTitle;
    post.longDescription = longDescription;
    post.slug = slug;
    post.status = status;

    try {
        await post.save();
        return { post };
    } catch (err) {
        throw new HttpError(err, 500);
    }
};

/**
 * Deletes a post from the database.
 *
 * @param {string} postId - The ID of the post to delete.
 * @returns {Promise<boolean>} - A Promise that resolves to true if the post was deleted successfully.
 * @throws {HttpError} - If there was an error deleting the post.
 */
const destroy = async (postId) => {
    try {
        await Post.findById(postId).deleteOne().exec();
        return true;
    } catch (err) {
        throw new HttpError(err, 500);
    }
};

export default {
    get,
    store,
    find,
    findBySlug,
    update,
    destroy
};
