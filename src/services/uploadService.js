import multer from 'multer';
import path from 'path';
import fs from 'fs';
import { UPLOAD_DIR } from '../utils/constants.js';

/**
 * Middleware function to handle file uploads using multer.
 * @param {string} uploadFolder - The folder where the uploaded files will be stored.
 * @returns {function} - Multer middleware function.
 */
export default function imageUploader (uploadFolder) {
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            const destination = path.resolve(`${UPLOAD_DIR}/${uploadFolder}`);

            if (!fs.existsSync(destination)) {
                fs.mkdirSync(destination, { recursive: true });
            }

            cb(null, destination);
        },
        filename: function (req, file, cb) {
            const extension = file.mimetype.split('/')[1];

            const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
            cb(null, file.fieldname + '-' + uniqueSuffix + '.' + extension);
        }
    });

    return multer({ storage }).single(uploadFolder);
}
