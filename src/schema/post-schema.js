import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Mongoose schema for blog posts.
 * @typedef {Object} PostSchema
 * @property {Schema.Types.ObjectId} categoryId - The ID of the category the post belongs to.
 * @property {Schema.Types.ObjectId} subCategoryId - The ID of the subcategory the post belongs to.
 * @property {string} title - The title of the post.
 * @property {string} status - The status of the post.
 * @property {string} slug - The slug of the post.
 * @property {string} [subTitle] - The subtitle of the post.
 * @property {string} [longDescription] - The long description of the post.
 */
const postSchema = new Schema({
    categoryId: { type: Schema.Types.ObjectId, required: true },
    subCategoryId: { type: Schema.Types.ObjectId, required: true },
    title: { type: String, required: true },
    status: { type: String, required: true },
    slug: { type: String, required: true, unique: true },
    subTitle: { type: String, required: false },
    longDescription: { type: String, required: false }
});

postSchema.plugin(uniqueValidator);

export default postSchema;
