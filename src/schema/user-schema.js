import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * User schema for MongoDB.
 * @typedef {Object} UserSchema
 * @property {string} name - The name of the user.
 * @property {string} email - The email of the user.
 * @property {string} password - The password of the user.
 * @property {string} mobile - The mobile number of the user.
 * @property {string} dob - The date of birth of the user.
 * @property {string} gender - The gender of the user.
 * @property {string} image - The image of the user.
 * @property {string} address - The address of the user.
 */
const userSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, minlength: 6 },
    mobile: { type: String, required: true, minlength: 10 },
    dob: { type: String, required: true },
    gender: { type: String, required: true },
    image: { type: String, required: true },
    address: { type: String, required: true }
});

userSchema.plugin(uniqueValidator);

export default userSchema;
