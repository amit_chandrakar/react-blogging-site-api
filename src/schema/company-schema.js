import mongoose from 'mongoose';
const Schema = mongoose.Schema;

/**
 * Mongoose schema for the company collection.
 * @typedef {Object} CompanySchema
 * @property {string} appName - The name of the company's app.
 * @property {string} dateFormat - The date format used by the company.
 * @property {string} timeFormat - The time format used by the company.
 * @property {string} language - The language used by the company.
 * @property {string} miniDrawer - The mini drawer used by the company.
 * @property {string} logo - The logo used by the company.
 * @property {string} path - The path used by the company.
 */
const companySchema = new Schema({
    appName: { type: String, required: false },
    dateFormat: { type: String, required: false },
    timeFormat: { type: String, required: false },
    language: { type: String, required: false },
    miniDrawer: { type: String, required: false },
    logo: { type: String, required: false },
    path: { type: String, required: false }
});

export default companySchema;
