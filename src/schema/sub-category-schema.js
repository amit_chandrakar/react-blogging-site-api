import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Mongoose schema for subcategories.
 * @typedef {Object} SubCategorySchema
 * @property {Schema.Types.ObjectId} categoryId - The ID of the category this subcategory belongs to.
 * @property {string} name - The name of the subcategory.
 * @property {string} status - The status of the subcategory.
 */
const subCategorySchema = new Schema({
    categoryId: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true, unique: true },
    status: { type: String, required: true }
});

subCategorySchema.plugin(uniqueValidator);

export default subCategorySchema;
