import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Mongoose schema for categories.
 * @typedef {Object} CategorySchema
 * @property {string} name - The name of the category.
 * @property {string} status - The status of the category.
 */
const categorySchema = new Schema({
    name: { type: String, required: true, unique: true },
    status: { type: String, required: true }
});

categorySchema.plugin(uniqueValidator);

export default categorySchema;
