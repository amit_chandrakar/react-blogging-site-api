import HttpError from '../services/httpErrorService.js';
import PostService from '../services/dbServices/postService.js';
import Post from '../models/post-model.js';

/**
 * Retrieves a list of posts based on the query parameters.
 *
 * @function
 * @async
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @param {Function} next - Express next middleware function.
 * @returns {Promise<void>} - Promise that resolves with the response object.
 * @throws {HttpError} - Throws an error if there was an issue retrieving the posts.
 */
const get = async (req, res, next) => {
    try {
        const posts = await PostService.get(req);
        res.json(posts);
    } catch (error) {
        const err = new HttpError(error, 500);
        return next(err);
    }
};

/**
 * Add a new post to the database.
 *
 * @function
 * @async
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @param {Function} next - Express next middleware function.
 * @returns {Promise<void>} - Promise that resolves with the new post object.
 * @throws {HttpError} - Throws an error if there was a problem saving the post to the database.
 */
const store = async (req, res, next) => {
    try {
        const post = await PostService.store(req.body);
        res.json({ status: 'success', message: 'Post added successfully', data: post });
    } catch (err) {
        const error = new HttpError(err, 500);
        return next(error);
    }
};

/**
 * Edit a post by ID
 *
 * @function
 * @async
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @throws {HttpError} If an error occurs while fetching the post
 * @returns {Object} JSON response containing the edited post
 */
const edit = async (req, res, next) => {
    const postId = req.params.postId;

    try {
        const post = await PostService.find(postId);
        res.json({ status: 'success', message: 'Post retrieved successfully', data: post });
    } catch (err) {
        const error = new HttpError(err, 500);
        return next(error);
    }
};

/**
 * Updates a post with the given postId in the database.
 * @async
 * @function
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @param {Function} next - The next middleware function.
 * @throws {HttpError} Will throw an error if there is an issue finding or saving the post.
 * @returns {Object} Returns a JSON object containing the updated post.
 */
const update = async (req, res, next) => {
    try {
        const result = await PostService.update(req.params.postId, req.body);
        res.json({ status: 'success', message: 'Post updated successfully', data: result });
    } catch (error) {
        const err = new HttpError(error, 500);
        return next(err);
    }
};

/**
 * Deletes a post with the specified ID.
 *
 * @function
 * @async
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @param {Function} next - The next middleware function.
 * @throws {HttpError} If an error occurs while deleting the post.
 * @returns {Object} The response object with a success message.
 */
const destroy = async (req, res, next) => {
    try {
        await PostService.destroy(req.params.postId);
        res.json({ status: 'success', message: 'Post deleted successfully' });
    } catch (error) {
        const err = new HttpError(error, 500);
        return next(err);
    }
};

/**
 * Retrieves a post by its slug.
 *
 * @function
 * @async
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @param {Function} next - Express next middleware function.
 * @returns {Object} - JSON response containing the retrieved post.
 * @throws {HttpError} - If an error occurs while retrieving the post.
 */
const findBySlug = async (req, res, next) => {
    const slug = req.params.slug;

    try {
        const post = await PostService.findBySlug(slug);
        res.json({ status: 'success', message: 'Post retrieved successfully', data: post });
    } catch (err) {
        const error = new HttpError(err, 500);
        return next(error);
    }
};

/**
 * Counts the total number of posts.
 * @function
 * @async
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @param {Function} next - The next middleware function.
 * @returns {Object} - The total number of posts.
 */
const count = async (req, res, next) => {
    let totalPosts = 0;

    try {
        totalPosts = await Post.count();
    } catch (err) {
        const error = new HttpError(err, 500);
        return next(error);
    }

    res.json({ totalPosts });
};

export default {
    get,
    store,
    edit,
    update,
    destroy,
    findBySlug,
    count
};
