import 'dotenv/config';
import HttpError from '../services/httpErrorService.js';
import createRazorpayOrder from '../services/razorpayService.js';

/**
 * Creates a new order using Razorpay API.
 *
 * @function
 * @async
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @param {Function} next - The next middleware function.
 * @returns {Promise<void>} - A Promise that resolves with the created order object or rejects with an error.
 * @throws {HttpError} - If order creation fails.
 */
const createOrder = async (req, res, next) => {
    const amount = 50000;
    const currency = 'INR';
    const receipt = 'order_rcptid_11';

    try {
        const order = await createRazorpayOrder(amount, currency, receipt);

        if (!order) {
            const error = new HttpError('Order creation failed', 500);
            return next(error);
        }

        res.json(order);
    } catch (error) {
        const err = new HttpError(error.message, 500);
        return next(err);
    }
};

export default {
    createOrder
};
