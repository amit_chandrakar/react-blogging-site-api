import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import 'dotenv/config';
import routes from './routes/index.js';
import { API_VERSION } from './config/app.js';
import { UPLOAD_DIR } from './utils/constants.js';
import mongoose from 'mongoose';
import logger from './services/loggerService.js';
const app = express();

const PORT = process.env.PORT || process.env.APP_PORT;

app.use(bodyParser.json());
app.use(cors());

// Image Upload to local storage
app.use('/public', express.static('public'));
app.use(express.static(UPLOAD_DIR));

app.use(API_VERSION, routes);

mongoose.connect(`${process.env.MONGO_URL}${process.env.MONGO_DBNAME}${process.env.MONGO_QUERY_PARAMS}`)
    .then(() => {
        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
            logger.info(`Server is running on port ${PORT}`);
        });
    })
    .catch(err => {
        console.log(err);
    });
