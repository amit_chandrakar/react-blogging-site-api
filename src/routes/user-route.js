import express from 'express';
import userController from '../controllers/user-controller.js';

const router = express.Router();

router.get('/', userController.get);
router.post('/store', userController.store);
router.get('/edit/:userId', userController.edit);
router.put('/update/:userId', userController.update);
router.delete('/delete/:userId', userController.destroy);

export default router;
