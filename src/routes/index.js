import categoryRoute from './category-route.js';
import subCategoryRoute from './sub-category-route.js';
import postRoute from './post-route.js';
import companyRoute from './company-route.js';
import userRoute from './user-route.js';
import paymentRoute from './payment-route.js';
import authRoute from './auth-route.js';
import express from 'express';
import verifyToken from '../middleware/verifyToken.js';
import HttpError from '../services/httpErrorService.js';
import fs from 'fs';
const app = express();

app.use('/auth', authRoute);
app.use('/payments', paymentRoute);

app.use('/posts', postRoute);

// Apply verifyToken middleware globally
app.use(verifyToken);
app.use('/users', userRoute);
app.use('/categories', categoryRoute);
app.use('/sub-categories', subCategoryRoute);
app.use('/companies', companyRoute);

app.use((req, res, next) => {
    const error = new HttpError('Could not find this route.', 404);
    throw error;
});

app.use((error, req, res, next) => {
    if (req.file) {
        fs.unlink(req.file.path, err => {
            console.log(err);
        });
    }
    if (res.headerSent) {
        return next(error);
    }

    res.status(error.code || 500);

    const response = {
        status: 'error',
        message: error.message || 'An unknown error occurred!'
    };

    res.json(response);
});

export default app;
