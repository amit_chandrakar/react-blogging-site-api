import express from 'express';
import categoryController from '../controllers/category-controller.js';

const router = express.Router();

router.get('/', categoryController.get);
router.post('/store', categoryController.store);
router.get('/edit/:categoryId', categoryController.edit);
router.put('/update/:categoryId', categoryController.update);
router.delete('/delete/:categoryId', categoryController.destroy);
router.get('/total-categories', categoryController.count);

export default router;
