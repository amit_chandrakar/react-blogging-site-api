import express from 'express';
import postController from '../controllers/post-controller.js';

const router = express.Router();

router.get('/', postController.get);
router.post('/store', postController.store);
router.get('/edit/:postId', postController.edit);
router.get('/show/:slug', postController.findBySlug);
router.put('/update/:postId', postController.update);
router.delete('/delete/:postId', postController.destroy);

export default router;
