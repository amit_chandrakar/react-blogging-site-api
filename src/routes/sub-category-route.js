import express from 'express';
import subCategoryController from '../controllers/sub-category-controller.js';

const router = express.Router();

router.get('/', subCategoryController.get);
router.post('/store', subCategoryController.store);
router.get('/edit/:subCategoryId', subCategoryController.edit);
router.put('/update/:subCategoryId', subCategoryController.update);
router.delete('/delete/:subCategoryId', subCategoryController.destroy);
router.get('/get-sub-categories-by-category-id/:categoryId', subCategoryController.getSubCategoriesByCategoryId);
router.get('/total-sub-categories', subCategoryController.count);

export default router;
