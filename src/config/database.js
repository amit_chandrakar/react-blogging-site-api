import express from 'express';
import mongoose from 'mongoose';
import 'dotenv/config';
import logger from '../services/loggerService.js';
const app = express();

export default function connect () {
    mongoose.connect(`${process.env.MONGO_URL}${process.env.MONGO_DBNAME}${process.env.MONGO_QUERY_PARAMS}`)
        .then(() => {
            app.listen(process.env.PORT, () => {
                console.log(`Server is running on port ${process.env.PORT}`);
                logger.info(`Server is running on port ${process.env.PORT}`);
            });
        })
        .catch(err => {
            console.log(err);
        });
}
