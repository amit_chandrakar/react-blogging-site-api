import mongoose from 'mongoose';
import postSchema from '../schema/post-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Post', postSchema); // (<CollectionName>, <CollectionSchema>)
