import mongoose from 'mongoose';
import subCategorySchema from '../schema/sub-category-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('SubCategory', subCategorySchema); // (<CollectionName>, <CollectionSchema>)
