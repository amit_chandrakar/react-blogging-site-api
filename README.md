## React Blogging Site API

<p>This is a blogging site API built with Node.js, Express.js, MongoDB, and Mongoose. It is a RESTful API that allows you to create, read, update, and delete blog posts. It also allows you to create, read, update, and delete comments on blog posts. It is built with MVC architecture and uses MongoDB Atlas for the database. It is deployed on Heroku.</p>

<p>
    <a href="https://www.mongodb.com/cloud/atlas" target="_blank">
        <img alt="MongoDB Atlas" src="https://img.shields.io/badge/MongoDB%20Atlas-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white"/>
    </a>
    <a href="https://expressjs.com/" target="_blank">
        <img alt="Express.js" src="https://img.shields.io/badge/Express.js-404D59?style=for-the-badge"/>
    </a>
    <a href="https://nodejs.org/en/" target="_blank">
        <img alt="Node.js" src="https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white"/>
    </a>
    <a href="https://www.heroku.com/" target="_blank">
        <img alt="Heroku" src="https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white"/>
    </a>
</p>

## Installation

1. Clone the repo
    ```sh
    git clone https://gitlab.com/amit_chandrakar/react-blogging-site-api.git
    ```
2. Install NPM packages
    ```sh
     npm install
    ```
3. Run command to create .env file
    ```sh
     cp .env.example .env
    ```
4. Enter your MongoDB Atlas URI in `.env`
    ```JS
    MONGO_URI = 'ENTER YOUR MONGO URI';
    ```
5. Run the app
    ```sh
    npm start
    ```
6. Test the API with Postman or Insomnia. The API documentation URL is <a href="https://documenter.getpostman.com/view/4333189/2s9YRCVAhD" target="_blank">https://documenter.getpostman.com/view/4333189/2s9YRCVAhD</a>


## Features of this API

<p>The following features has been planned for this API.</p>

<table width="100%">
    <thead>
        <tr>
            <th>Feature</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                CRUD operations for blog categories, blog sub-categories, blog posts, and comments on blog posts. Also, it has filters, pagination, and sorting for blog categories, blog sub-categories, blog posts.
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <tr>
            <td>
                File/Image upload
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <tr>
            <td>
                Seeder
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <tr>
            <td>
                <li>Authentication</li>
                <ul>
                    <li>Login</li>
                    <li>Register</li>
                    <li>Forgot password</li>
                    <li>Reset password</li>
                    <li>Email verification</li>
                    <li>Social login</li>
                </ul>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-In Progress-yellow" alt="In Progress" />
            </td>
        </tr>
        <tr>
            <td>
                <li>Integrations</li>
                <ul>
                    <li>Docker</li>
                    <li>CI/CD</li>
                    <li>Linting and Code Formatting</li>
                    <li>Code Documentation</li>
                </ul>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-In Progress-yellow" alt="In Progress" />
            </td>
        </tr>
        <tr>
            <td>
                <li>Payment gateway</li>
                <ul>
                    <li>Stripe</li>
                    <li>Paypal</li>
                    <li>Razorpay</li>
                </ul>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-In Progress-yellow" alt="In Progress" />
            </td>
        </tr>
        <tr>
            <td>
                SMS Gateway
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                Google recaptcha
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                Chat
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                Google Calendar
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                Google maps
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                Database backup
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                Zoom integration
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                User Activity Logs
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
        <tr>
            <td>
                User Roles and Permissions
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Pending-orange" alt="Pending" />
            </td>
        </tr>
    </tbody>
</table>

## Node packages used

<p>These are the packages used in this project. Click on the package name to go to the package page.</p>

<table width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Package Link</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <!-- express -->
        <tr>
            <td>
                <a href="javascript:;">Express</a>
            </td>
            <td>
                Fast, unopinionated, minimalist web framework for node.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/express" target="_blank">
                    <img alt="Express" src="https://img.shields.io/badge/express-4.17.1-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- mongoose -->
        <tr>
            <td>
                <a href="javascript:;">Mongoose</a>
            </td>
            <td>
                Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/mongoose" target="_blank">
                    <img alt="Mongoose" src="https://img.shields.io/badge/mongoose-6.0.11-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- dotenv -->
        <tr>
            <td>
                <a href="javascript:;">Dotenv</a>
            </td>
            <td>
                Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/dotenv" target="_blank">
                    <img alt="Dotenv" src="https://img.shields.io/badge/dotenv-10.0.0-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- nodemon -->
        <tr>
            <td>
                <a href="javascript:;">Nodemon</a>
            </td>
            <td>
                Nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/nodemon" target="_blank">
                    <img alt="Nodemon" src="https://img.shields.io/badge/nodemon-2.0.13-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- morgan -->
        <tr>
            <td>
                <a href="javascript:;">Morgan</a>
            </td>
            <td>
                HTTP request logger middleware for node.js.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/morgan" target="_blank">
                    <img alt="Morgan" src="https://img.shields.io/badge/morgan-1.10.0-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- cors -->
        <tr>
            <td>
                <a href="javascript:;">Cors</a>
            </td>
            <td>
                CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/cors" target="_blank">
                    <img alt="Cors" src="https://img.shields.io/badge/cors-2.8.5-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- winston logger -->
        <tr>
            <td>
                <a href="javascript:;">Winston</a>
            </td>
            <td>
                A logger for just about everything.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/winston" target="_blank">
                    <img alt="Winston" src="https://img.shields.io/badge/winston-3.3.3-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- slugify -->
        <tr>
            <td>
                <a href="javascript:;">Slugify</a>
            </td>
            <td>
                Slugifies string with replacement.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/slugify" target="_blank">
                    <img alt="Slugify" src="https://img.shields.io/badge/slugify-1.4.0-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- razorpay -->
        <tr>
            <td>
                <a href="javascript:;">Razorpay</a>
            </td>
            <td>
                Razorpay Node.js client for API v1 and v2.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/razorpay" target="_blank">
                    <img alt="Razorpay" src="https://img.shields.io/badge/razorpay-2.2.1-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- nodemailer -->
        <tr>
            <td>
                <a href="javascript:;">Nodemailer</a>
            </td>
            <td>
                Send e-mails from Node.js – easy as cake! 🍰✉️
            </td>
            <td>
                <a href="https://www.npmjs.com/package/nodemailer" target="_blank">
                    <img alt="Nodemailer" src="https://img.shields.io/badge/nodemailer-6.6.3-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- multer -->
        <tr>
            <td>
                <a href="javascript:;">Multer</a>
            </td>
            <td>
                Multer is a node.js middleware for handling multipart/form-data, which is primarily used for uploading files.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/multer" target="_blank">
                    <img alt="Multer" src="https://img.shields.io/badge/multer-1.4.3-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- jsonwebtoken -->
        <tr>
            <td>
                <a href="javascript:;">Jsonwebtoken</a>
            </td>
            <td>
                An implementation of JSON Web Tokens.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/jsonwebtoken" target="_blank">
                    <img alt="Jsonwebtoken" src="https://img.shields.io/badge/jsonwebtoken-8.5.1-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- eslint -->
        <tr>
            <td>
                <a href="javascript:;">Eslint</a>
            </td>
            <td>
                A pluggable and configurable linter tool for identifying and reporting on patterns in JavaScript.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/eslint" target="_blank">
                    <img alt="Eslint" src="https://img.shields.io/badge/eslint-7.32.0-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- husky -->
        <tr>
            <td>
                <a href="javascript:;">Husky</a>
            </td>
            <td>
                Husky can prevent bad git commit, git push and more 🐶 woof!
            </td>
            <td>
                <a href="https://www.npmjs.com/package/husky" target="_blank">
                    <img alt="Husky" src="https://img.shields.io/badge/husky-7.0.4-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-Done-green" alt="Done" />
            </td>
        </tr>
        <!-- migrate-mongo -->
        <tr>
            <td>
                <a href="javascript:;">Migrate-mongo</a>
            </td>
            <td>
                The MongoDB CLI for Migrations.
            </td>
            <td>
                <a href="https://www.npmjs.com/package/migrate-mongo" target="_blank">
                    <img alt="Migrate-mongo" src="https://img.shields.io/badge/migrate--mongo-9.3.0-blue" />
                </a>
            </td>
            <td>
                <img src="https://img.shields.io/badge/-In Progress-yellow" alt="In Progress" />
            </td>
        </tr>
    </tbody>
</table>

## API Documentation

<p>The API documentation URL is <a href="https://documenter.getpostman.com/view/4333189/2s9YRCVAhD" target="_blank">https://documenter.getpostman.com/view/4333189/2s9YRCVAhD</a></p>

## License

<p>
This is an open source project. Feel free to do whatever you want with it. 🤘😎🤘
</p>

