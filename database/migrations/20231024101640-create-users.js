export async function up (db, client) {
    const users = [
        {
            name: 'Admin',
            email: 'admin@examlpe.com',
            password: '123456',
            dob: '1990-01-01',
            gender: 'male',
            mobile: '1234567890',
            address: 'Test Address',
            image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg'
        },
        {
            name: 'John Doe',
            email: 'john@example.com',
            password: '123456',
            dob: '1991-03-05',
            gender: 'male',
            mobile: '8907654321',
            address: 'Test Address',
            image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg'
        },
        {
            name: 'Alisa Doe',
            email: 'alisa@example.com',
            password: '123456',
            dob: '1995-03-23',
            gender: 'female',
            mobile: '90877654321',
            address: 'Test Address',
            image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg'
        }

    ];

    await db.collection('users').insertMany(users);
}

export async function down (db, client) {
    // Delete created users
    await db.collection('users').deleteMany({ email: { $in: ['admin@examlpe.com', 'john@example.com', 'alisa@example.com'] } });
}
